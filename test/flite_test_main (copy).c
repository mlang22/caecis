#include <stdio.h>
#include <string.h>
#include "flite/flite.h"

void usenglish_init(cst_voice *v);
cst_lexicon *cmulex_init(void);


int say(char *text)
{

    cst_voice *v;
    cst_wave *w;
    cst_utterance *u;

    //voice_pathname = argv[1]; /* pathname to .flitevox file */
    const char *outfile = "play";           /* text to be synthesized */

    /* Initialize Flite, and set up language and lexicon */
    flite_init();
    flite_add_lang("eng",usenglish_init,cmulex_init);
    flite_add_lang("usenglish",usenglish_init,cmulex_init);

    /* Load and select voice */
    v = flite_voice_select("cmu_us_aew.flitevox");
    if (v == NULL)
    {
        fprintf(stderr,"failed to load voice: %s\n","cmu_us_aew.flitevox");
        return 1;
    }

    u = flite_synth_text(text,v);
    w = utt_wave(u);

    /* Play the resulting wave, save it to a filename, or do none of these */
    if (cst_streq(outfile,"play"))
        play_wave(w);
    else if (!cst_streq(outfile,"none"))
       cst_wave_save_riff(w,outfile);

    delete_utterance(u); /* will delete w too */

    return 0;
}

int main()
{
  
  char* test = "hi";
  say(test);
}

